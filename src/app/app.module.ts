import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/homePage/homePage';
import { Page2 } from '../pages/page2/page2';
import { CameraPage } from '../pages/cameraPage/cameraPage';
import { BasicAlert } from '../helpers/alerts/basicalert/basicalert'
import {BasicActionSheet} from "../helpers/actionsheets/basicactionsheet/basicationsheet";
import {BasicModalHelper} from "../helpers/modals/basicmodal/basicmodalhelper";
import {ModalContentPage} from "../helpers/modals/basicmodal/basicmodal";
import {PasswordAlert} from "../helpers/alerts/passwordalert/passwordalert";
import {ModalPage} from "../pages/modalPage/modalPage";
import {ActionSheetPage} from "../pages/actionSheetPage/actionSheetPage";
import {LoginPage} from "../pages/loginPage/loginPage";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Page2,
    ModalPage,
    ActionSheetPage,
    LoginPage,
    CameraPage,
    ModalContentPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Page2,
    ModalPage,
    ActionSheetPage,
    LoginPage,
    CameraPage,
    ModalContentPage
  ],
  providers: [
    BasicAlert,
    BasicActionSheet,
    BasicModalHelper,
    PasswordAlert
  ]
})
export class AppModule {}
