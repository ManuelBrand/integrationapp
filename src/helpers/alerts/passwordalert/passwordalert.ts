import {Injectable} from "@angular/core";
import {AlertController} from "ionic-angular";

@Injectable()
export class PasswordAlert {

  constructor(private alertCtrl: AlertController) {}

  showPasswordAlert(): void {
    let prompt = this.alertCtrl.create({
      title: 'Forgot password',
      message: "Please enter your e-mail so we can send you a new password.",
      inputs: [
        {
          name: 'e-mail',
          placeholder: 'bijvoorbeeld@etcetera.nl'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            console.log('Send clicked');
          }
        }
      ]
    });
    prompt.present();
  }
}
