/**
 * Created by manbrand on 28/10/16.
 */

import {Injectable} from "@angular/core";
import {AlertController} from "ionic-angular";

@Injectable()
export class BasicAlert {

  constructor(private alertCtrl : AlertController) {
  }

  showBasicAlert(): void {
    let alert = this.alertCtrl.create({
      title: 'Basic Alert',
      message: 'This is a basic alert.',
      buttons: [
        {
          text: 'Cancel',
          role: "cancel",
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          role: 'accept',
          handler: () => {
            console.log('Agree clicked');
          }
        }
      ]
    });

    alert.present();
  }
}
