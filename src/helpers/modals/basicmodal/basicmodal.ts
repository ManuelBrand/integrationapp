import {Component} from "@angular/core";
import {ViewController, Platform} from "ionic-angular";

@Component({
  templateUrl: 'basicmodal.html'
})

export class ModalContentPage {
  character;

  constructor(
    public platform: Platform,
    public viewCtrl: ViewController) {
  }

  modalObj: any[]  = [
    {
      title: 'Uno',
      note: 'Aap'
    },
    {
      title: 'Dos',
      note: 'Noot'
    },
    {
      title: 'Tres',
      note: 'Mies'
    }
  ];

  dismiss(): void {
    this.viewCtrl.dismiss();
  }
}
