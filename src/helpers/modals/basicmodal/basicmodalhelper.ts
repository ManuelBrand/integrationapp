import {Injectable} from "@angular/core";
import {ModalController} from "ionic-angular";
import {ModalContentPage} from "./basicmodal";

@Injectable()
export class BasicModalHelper {
  constructor(public modalCtrl: ModalController) {

  }

  showBasicModal() {
    let modal = this.modalCtrl.create(ModalContentPage);
    modal.present();
  }

}
