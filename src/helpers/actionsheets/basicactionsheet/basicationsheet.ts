import {Injectable} from "@angular/core";
import {ActionSheetController} from "ionic-angular";

@Injectable()
export class BasicActionSheet {

  constructor(public actionSheetCtrl: ActionSheetController) {
  }

  showBasicActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Modify your album',
      buttons: [
        {
          text: 'Option 1',
          role: 'destructive',
          handler: () => {
            console.log('Option 1 Clicked');
          }
        },{
          text: 'Option 2',
          handler: () => {
            console.log('Option 2 Clicked');
          }
        },{
          text: 'Option 3',
          handler: () => {
            console.log('Option 3 Clicked');
          }
        },{
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
}
