import { Component } from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {Page2} from "../page2/page2";
import {BasicAlert} from "../../helpers/alerts/basicalert/basicalert";
import {BasicActionSheet} from "../../helpers/actionsheets/basicactionsheet/basicationsheet";
import {BasicModalHelper} from "../../helpers/modals/basicmodal/basicmodalhelper";
import {PasswordAlert} from "../../helpers/alerts/passwordalert/passwordalert";


@Component({
  selector: 'page-page1',
  providers: [BasicAlert],
  templateUrl: 'homePage.html'
})
export class HomePage {

  constructor(private alertCtrl: AlertController,
              private navCtrl: NavController,
              private basicAlert: BasicAlert,
              private basicActionSheet: BasicActionSheet,
              private basicModalHelper: BasicModalHelper,
              private passwordAlert: PasswordAlert) {

  }

  showBasicAlert(): void {
    this.basicAlert.showBasicAlert();
  }

  showBasicModal(): void {
    this.basicModalHelper.showBasicModal();
  }

  showBasicActionSheet(): void {
    this.basicActionSheet.showBasicActionSheet();
  }

  showPasswordAlert(): void {
    this.passwordAlert.showPasswordAlert();
  }

  goToNextPage(): void {
    this.navCtrl.push(Page2);
  }
}
