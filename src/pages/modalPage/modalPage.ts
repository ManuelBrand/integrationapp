import {Component} from "@angular/core";
import {BasicModalHelper} from "../../helpers/modals/basicmodal/basicmodalhelper";

@Component({
  selector: 'page-modalPage',
  providers: [BasicModalHelper],
  templateUrl: 'modalPage.html'
})
export class ModalPage {

  constructor(private basicModalHelper: BasicModalHelper){

  }

  showBasicModal(): void {
    this.basicModalHelper.showBasicModal();
  }
}
