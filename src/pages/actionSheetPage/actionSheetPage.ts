import {Component} from "@angular/core";
import {BasicActionSheet} from "../../helpers/actionsheets/basicactionsheet/basicationsheet";

@Component({
  selector: 'page-actionSheetPage',
  providers: [BasicActionSheet],
  templateUrl: 'actionSheetPage.html'
})
export class ActionSheetPage {

  constructor(private basicActionSheet: BasicActionSheet){
  }

  showBasicActionSheet(): void {
    this.basicActionSheet.showBasicActionSheet();
  }


}
