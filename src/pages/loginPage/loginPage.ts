import {Component} from "@angular/core";
import {PasswordAlert} from "../../helpers/alerts/passwordalert/passwordalert";

@Component({
  selector: 'page-loginPage',
  providers: [],
  templateUrl: 'loginPage.html'
})
export class LoginPage {

  constructor(private passwordAlert: PasswordAlert){
  }

  showPasswordAlert(): void {
    this.passwordAlert.showPasswordAlert();
  }

}
