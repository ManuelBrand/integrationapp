# README #

This Repo contains code used for the project MoreApp Integration Testing.

### What is this repository for? ###

Repo contains:

* Hybrid Ionic Application for testing purposes,
* Protractor tests to run with Appium (Appium.io).
* v0.2

### How do I get set up? ###

* -- Build-scripts will follow soon
* Dependencies:
* Ionic 2 (ionic.io)
* Angular 2 (angular.io)
* Protractor (protractortest.org/)
* Physical Device (Android or iOS)
* Android: Java JDK, Android SDK
* iOS: XCode 8+

 
Manuel Brand student at HAN 474283