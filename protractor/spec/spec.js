describe('Protractor tests for IntegrationTest App', function() {

  var hamburgerButton = element(by.id('menuButton'));
  var hamburgerPage2Button = element(by.cssContainingText('.input-wrapper', 'Page Two'));
  var hamburgerCameraPageButton = element(by.cssContainingText('.input-wrapper', 'CameraPage'));
  var hamburgerModalPageButton = element(by.cssContainingText('.input-wrapper', 'Modal Page'));
  var hamburgerActionSheetPageButton = element(by.cssContainingText('.input-wrapper', 'ActionSheet Page'));
  var hamburgerLoginPageButton = element(by.cssContainingText('.input-wrapper', 'Login Page'));

  var menuBanner = element(by.cssContainingText('ion-title', 'Menu'));

  var alertButton = element(by.id('basicAlertButton'));
  var alertMessage = element(by.css('.alert-message'));
  var alertConfirmButton = element(by.cssContainingText('.button-inner', 'Ok'));

  var nextPageButton = element(by.id('nextPageButton'));
  var page2MenuBanner = element(by.cssContainingText('.toolbar-title', 'Page Two'));

  var modalButton = element(by.id('basicModalButton'));
  var modalPageBanner = element(by.cssContainingText('.toolbar-title', 'Description'));
  var closeModalButton = element(by.id('closeModalButton'));

  var actionsheetButton = element(by.id('basicActionsheetButton'));
  var actionsheetTitle = element(by.cssContainingText('.action-sheet-title', 'Modify your album'));
  var actionsheetOptionButton = element(by.cssContainingText('.button-inner', 'Option 1'));

  var forgotPasswordButton = element(by.id('forgotPasswordButton'));
  var forgotPasswordEmailInput = element(By.css('.alert-input'));
  var forgotPasswordSendButton = element(by.cssContainingText('.button-inner', 'Send'));

  var cameraPageMenuBanner = element(by.cssContainingText('.toolbar-title', 'Camera Page'));
  var cameraButton = element(by.name('camera'));

  function openMenu() {
    hamburgerButton.click();
    browser.driver.sleep(500);
  }

  function sleep() {
    browser.driver.sleep(500);
  }

  beforeEach(function() {
    browser.executeScript('window.name = "NG_ENABLE_DEBUG_INFO!"');
  });

  it('1. should have the right title', function() {
    expect(browser.getTitle()).toEqual('Home');
  });

  it('2. should show basic ionic alert', function() {
    alertButton.click();
    browser.waitForAngular();
    expect(alertMessage.getText()).toBe('This is a basic alert.');
    alertConfirmButton.click();
  });

  it('3. Should show basic ionic modal', function () {
    openMenu();
    hamburgerModalPageButton.click();
    sleep();
    browser.waitForAngular();
    modalButton.click();
    expect(modalPageBanner.isDisplayed()).toBe(true);
    closeModalButton.click();
  });

  it('4. Should open basic ionic actionsheet', function() {
    openMenu();
    hamburgerActionSheetPageButton.click();
    sleep();
    actionsheetButton.click();
    browser.waitForAngular();
    expect(actionsheetTitle.isDisplayed()).toBe(true);
    actionsheetOptionButton.click();
    sleep();
  });

  it('5. should request a new password', function() {
    openMenu();
    hamburgerLoginPageButton.click();
    sleep();
    browser.waitForAngular();
    forgotPasswordButton.click();
    expect(alertMessage.getText()).toBe('Please enter your e-mail so we can send you a new password.');
    forgotPasswordEmailInput.sendKeys("lolers@roflcopter.lmao");
    forgotPasswordSendButton.click();
  });

  it('6. should have a sidemenu', function() {
    sleep();
    openMenu();
    sleep();
    expect(menuBanner.isDisplayed()).toBe(true);
  });

  it('7. should click a menu item on page2', function() {
    sleep();
    hamburgerPage2Button.click();
    expect(page2MenuBanner.isDisplayed()).toBe(true);
  });

  it('8. should open the CameraPage', function() {
    openMenu();
    hamburgerCameraPageButton.click();
    sleep();
    expect(cameraPageMenuBanner.isDisplayed()).toBe(true);
    cameraButton.click();

  });

});
