// exports.config = {
//   framework: 'jasmine',
//   seleniumAddress: 'http://localhost:4444/wd/hub',
//   specs: ['../spec/spec.js'],
//   multiCapabilities: [{
//     browserName: 'chrome'
//   }],
//   useAllAngular2AppRoots: true
// };
//
//
// /Users/manbrand/Workspaces/Tests/IntegrationApp/platforms/android/build/outputs/apk/android-debug.apk


exports.config = {


  /*
   * normally protractor runs tests on localhost:4444, but we want protractor to connect to appium
   * which runs on localhost:4723
   */


  seleniumAddress: 'http://localhost:4723/wd/hub',
  specs: ['../spec/spec.js'],


  /*
   * Some capabilities must be set to make sure appium can connect to your device.
   * platformVersion: this is the version of android
   * deviceName: your actual device name
   * browserName: leave this empty, we want protractor to use the embedded webview
   * autoWebView: set this to true for hybrid applications
   * app: the location of the apk (must be absolute)
   */


  capabilities: {

    /*
    Android Specs down here
     */

    // -- Commands needed to run properly, Appium
    // appium --chromedriver-executable /Users/manbrand/Downloads/chromedriver

    platformName: 'android',
    platformVersion: '7.0',
    deviceName: 'Nexus 5X',  //00fc2afbf174b3b7
    browserName: "",
    autoWebview: true,
    clearSystemFiles: true,
    app: '/Users/manbrand/Workspaces/Tests/IntegrationApp/platforms/android/build/outputs/apk/android-debug.apk'

    /*
    iOS Specs down here
     */

    // -- Commands needed to run properly, Appium & WebKit
    // appium --automation-name XCUITest
    // ios_webkit_debug_proxy -c f25795d4af51164c66c7f073cf0af59e5b015263:27753 -d

    // platformName: 'ios',
    // platformVersion: '9.3.5',
    // deviceName: 'iPad 2',
    // browserName: "",
    // autoWebview: true,
    // BundleID: 'com.ionicframework.integrationapp181557',
    // udid: 'f25795d4af51164c66c7f073cf0af59e5b015263',
    // realDeviceLogger: '/usr/local/lib/node_modules/deviceconsole',
    // app: '/Users/manbrand/Workspaces/Tests/IntegrationApp/platforms/ios/build/emulator/IntegrationApp.app'

  },


  useAllAngular2AppRoots: true,

  baseUrl: 'http://10.0.2.2:8000',

  /* configuring wd in onPrepare
   * wdBridge helps to bridge wd driver with other selenium clients
   * See https://github.com/sebv/wd-bridge/blob/master/README.md
   */
  onPrepare: function () {
      var wd = require('wd'),
          protractor = require('protractor'),
          wdBridge = require('wd-bridge')(protractor, wd);
      wdBridge.initFromProtractor(exports.config);
  }
};


